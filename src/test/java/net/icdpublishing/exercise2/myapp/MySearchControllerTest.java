package net.icdpublishing.exercise2.myapp;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import net.icdpublishing.exercise2.myapp.customers.dao.HardcodedListOfCustomersImpl;
import net.icdpublishing.exercise2.myapp.customers.domain.Customer;
import net.icdpublishing.exercise2.myapp.customers.domain.CustomerType;
import net.icdpublishing.exercise2.searchengine.domain.Address;
import net.icdpublishing.exercise2.searchengine.domain.Person;
import net.icdpublishing.exercise2.searchengine.domain.Record;
import net.icdpublishing.exercise2.searchengine.domain.SourceType;
import net.icdpublishing.exercise2.searchengine.loader.DataLoader;
import net.icdpublishing.exercise2.searchengine.requests.SimpleSurnameAndPostcodeQuery;
import net.icdpublishing.exercise2.searchengine.services.DummyRetrievalServiceImpl;
import net.icdpublishing.exercise2.searchengine.services.SearchEngineRetrievalService;

import org.junit.Before;
import org.junit.Test;

public class MySearchControllerTest {
	private MySearchController controller;
	private Person searchingFor = null;


	@Before
	public void setUp() throws Exception {

		DataLoader loader = new DataLoader();
		loader.loadAllDatasets();
		DummyRetrievalServiceImpl searchService = new DummyRetrievalServiceImpl(
				loader);
		controller = new MySearchController(searchService);

		searchingFor = new Person();
		searchingFor.setForename("Mary");
		searchingFor.setMiddlename("Ann");
		searchingFor.setSurname("Smith");
		searchingFor.setTelephone("07702811339");
		
		Address address2 = new Address();
		address2.setBuildnumber("13");
		address2.setPostcode("sw6 2bq");
		address2.setStreet("william morris way");
		address2.setTown("London");
		searchingFor.setAddress(address2);
		
		Set<SourceType> sources = new HashSet<SourceType>();
		sources.add(SourceType.BT);
	}

	/*
	 * Requirement 3 is being tested throughout testRequirement1 and testRequirement2
	 */
	
	@Test
	public void testRequirement1() {
		
		Customer customer = new Customer();
		customer.setEmailAddress("harry.lang@192.com");
		customer.setForename("Harry");
		customer.setSurname("Lang");
		customer.setCustomType(CustomerType.NON_PAYING);
				
		SearchRequest query = new SearchRequest(new SimpleSurnameAndPostcodeQuery(
				"Smith", "sw6 2bq"), customer);
	
		Collection<Record> results = controller.handleRequest(query);
		for(Record r:results)
			assertEquals(r.getPerson(), searchingFor);
		

	}

	@Test
	public void testRequirement2() {
		
		Customer customer = new Customer();
		customer.setEmailAddress("john.doe@192.com");
		customer.setForename("John");
		customer.setSurname("Doe");
		customer.setCustomType(CustomerType.PREMIUM);
		
		SimpleSurnameAndPostcodeQuery searchQuery = new SimpleSurnameAndPostcodeQuery(
				"Smith", "sw6 2bq");
		SearchRequest query = new SearchRequest(searchQuery, customer);
		
		Collection<Record> results = controller.handleRequest(query);

		for (Record r : results)
			System.out.println(r.getPerson().getForename());
		assertEquals(results.size(), 2);
	}

}
