package net.icdpublishing.exercise2.myapp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import net.icdpublishing.exercise2.myapp.charging.ChargingException;
import net.icdpublishing.exercise2.myapp.charging.services.ImaginaryChargingService;
import net.icdpublishing.exercise2.myapp.customers.dao.CustomerNotFoundException;
import net.icdpublishing.exercise2.myapp.customers.dao.HardcodedListOfCustomersImpl;
import net.icdpublishing.exercise2.myapp.customers.domain.Customer;
import net.icdpublishing.exercise2.myapp.customers.domain.CustomerType;
import net.icdpublishing.exercise2.searchengine.domain.Record;
import net.icdpublishing.exercise2.searchengine.domain.SourceType;

public class Requirements {

	private Collection<Record> results;
	private Customer customer;

	public Requirements(Collection<Record> r, Customer c) {
		results = r;
		customer = c;
	}

	// Requirement 1
	public void restrictionsForNonPayingUsers() {
		if (!customer.getCustomType().equals(CustomerType.NON_PAYING))
			return;
		for (Iterator<Record> iterator = results.iterator(); iterator.hasNext();) {
			Record r = iterator.next();
			if (r.getSourceTypes().size() > 1
					|| !r.getSourceTypes().contains(SourceType.BT))
				iterator.remove();

		}

	}

	// Requirement 2
	public void chargePremiumUsers() {
		if (!customer.getCustomType().equals(CustomerType.PREMIUM))
			return;
		List<Record> filtered = new ArrayList<Record>();
		int credit = 0;
		for (Record r : results) {
			if (r.getSourceTypes().size() > 1
					|| !r.getSourceTypes().contains(SourceType.BT)) {
				filtered.add(r);
				++credit;
				if (credit == 192) {
					break;
				}
			}

		}
		try {
			new ImaginaryChargingService().charge(customer.getEmailAddress(),
					credit);
		} catch (ChargingException e) {
			System.out.println(e.toString());
		}

		results = filtered;
	}

	// Requirements 3
	public void customerAuthentication() {
		Customer cust = null;
		try {
			cust = new HardcodedListOfCustomersImpl()
					.findCustomerByEmailAddress(customer.getEmailAddress());
		} catch (CustomerNotFoundException e) {
			System.out.println(e.toString());
		}
		if (!cust.equals(customer)) {
			System.out.println("Customer mismatch!");
		}
	}

	/*
	 * Requirement 4 
	 * In the project specification it is said to sort results
	 * based on surname, but each Record will have the same surname. Because of
	 * that I decided to sort the results in ascending order based on the
	 * attribute forename.
	 */

	public void orderByAscendingSurnames() {
		List<Record> list = new ArrayList<Record>(results);
		Comparator<Record> cmp = new Comparator<Record>() {
			@Override
			public int compare(Record c1, Record c2) {
				return c1.getPerson().getForename()
						.compareTo(c2.getPerson().getForename());

			}
		};

		Collections.sort(list, cmp);
		results = list;
	}

	public Collection<Record> filterResults() {
		// Requirement 1
		customerAuthentication();
		// Requirement 2
		restrictionsForNonPayingUsers();
		// Requirement 3
		chargePremiumUsers();
		// Requirement 4
		orderByAscendingSurnames();
		return results;
	}
}
